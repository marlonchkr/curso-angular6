export class DestinoViaje{
    private selected!: boolean; /*bandera privada para seleccionar */
    public servicios: string[];
	id: any;
    constructor(public nombre:string, u:string){
        this.servicios=['pileta','desayuno'];    }
    isSelected():boolean{
        return this.selected;
    }
    setSelected(s:boolean){
        this.selected= s;
    }

}