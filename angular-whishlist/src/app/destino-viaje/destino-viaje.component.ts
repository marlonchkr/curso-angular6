import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.model';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
/* variables*/
export class DestinoViajeComponent implements OnInit {
 @Input() destino!: DestinoViaje;
 @Input() position!: number;
 @HostBinding('attr.class') cssClass = 'col-md-4';/*Para remplazar el atributo clas que Ng me genera por defecto*/
 @Output() clicked: EventEmitter<DestinoViaje>; 
 /*defino variables*/
 constructor() {
   this.clicked=new EventEmitter();
 }
  ngOnInit(): void {
  }
  /*metodo*/
  ir(){
    this.clicked.emit(this.destino);
    return false;
  }
 }
