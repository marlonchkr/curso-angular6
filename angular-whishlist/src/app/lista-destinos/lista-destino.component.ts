import { Component, EventEmitter, Output, OnInit } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model';
import { DestinosApiClient } from './../models/destino-api-client.models';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];
  
  constructor(public destinosApiClient:DestinosApiClient) {
    this.onItemAdded = new EventEmitter();
    this.updates= [];
    this.destinosApiClient.subscribeOnChange((d: DestinoViaje) =>{
      if (d  != null){
        this.updates.push('Se ha elegido: ' + d.nombre);
      }
    });
  }

  ngOnInit(){}

 agregado(d: DestinoViaje) {
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
    
  }

  elegido(e:DestinoViaje) {
    this.destinosApiClient.elegir(e);
    /*this.destinosApiClient.getAll().forEach(x => x.setSelected(false));
    e.setSelected(true);*/
  }
}











/*import { Component, OnInit } from '@angular/core';
import {DestinoViaje } from './../models/destino-viaje.model';


@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  destinos:DestinoViaje[];
  constructor() { 
  this.destinos=[];
  }

  ngOnInit(): void {
  }
  /*funciones creo la funcion guardar que es la que recibe los dos eventos en el boton*/
 /* guardar(nombre:string, url:string):boolean{
    this.destinos.push(new DestinoViaje(nombre, url));
    return false;

  }
  elegido(d: DestinoViaje){
    this.destinos.forEach(function (x) {x.setSelected(false); });
    d.setSelected(true);
  }

}*/
